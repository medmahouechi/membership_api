from typing import Optional

from airtable import Airtable

from config.lib import from_env, throw

# CRM = Comrade Relational Manager
# This is an Airtable Base used to track new members for onboarding outreach by
# the SF CCC = Chapter Coordination Committee

# Note: Airtable class from airtable-python-wrapper automatically sets up
# auth from the AIRTABLE_API_KEY environment variable

# Identifier for the Base
CRM_ENABLED: Optional[str] = from_env.get_bool('CRM_ENABLED', False)
AIRTABLE_API_KEY: Optional[str] = from_env.get_str(
    'AIRTABLE_API_KEY', throw if CRM_ENABLED else 'fakekey')
CRM_BASE_KEY: Optional[str] = from_env.get_str(
    'CRM_BASE_KEY', throw if CRM_ENABLED else 'fakekey')
# Table ID for the new members table
CRM_COMRADES_TABLE: str = from_env.get_str(
    'CRM_COMRADES_TABLE', throw if CRM_ENABLED else 'faketable')
CRM_LINK: str = from_env.get_str('CRM_LINK', throw if CRM_ENABLED else None)

airtable = Airtable(base_key=CRM_BASE_KEY, table_name=CRM_COMRADES_TABLE, api_key=AIRTABLE_API_KEY)
