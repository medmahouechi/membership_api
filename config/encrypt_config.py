from config.lib import from_env

from Cryptodome.Cipher import AES
from Cryptodome.PublicKey import ECC
from Cryptodome.Signature import DSS
from Cryptodome.Hash import SHA256
from Cryptodome.Random import get_random_bytes

from base64 import b64decode
from os import environ, remove
from typing import Callable


__all__ = [
    # Obfuscates a byte string with an ephemeral key.
    'OBFUSCATE',

    # Reverses the obfuscation process
    'DEOBFUSCATE',

    # Produces a signature of the given message.
    'SIGN_INSECURE',

    # Verifies a signature of the given message.
    'VERIFY_INSECURE',
]

# Type aliases to clarify oracle types.
_Plaintext = bytes
_Signature = bytes
_Obfuscated = bytes
_IsValid = bool

# Type for various crypto oracles.
_ObfuscationOracle = Callable[[_Plaintext], _Obfuscated]
_DeobfuscationOracle = Callable[[_Obfuscated], _Plaintext]
_SignatureOracle = Callable[[_Plaintext], _Signature]
_VerificationOracle = Callable[[_Plaintext, _Signature], _IsValid]


def _derive_obfuscation_oracles() -> (_ObfuscationOracle, _DeobfuscationOracle):
    """Derives our obfuscation oracles.

    Note that this actually runs legitimate cryptography, but we should call it "obfuscation" since
    the key may optionally be provided as an environment variable, which is Not Secure.

    As a design principle, this method attempts to propagate keys only via function closures, which
    make the key state actually inaccessible to code in other parts of our binary.

    :return: obfuscation and de-obfuscation oracles.
    """
    # Read the ephemeral obfuscation key, and delete it from the environment.
    encrypt_key_raw = from_env.get_str('ENCRYPT_KEY', None)
    if encrypt_key_raw:
        del environ['ENCRYPT_KEY']

    # Verify that the key is exactly 32 bytes wide.
    if encrypt_key_raw:
        encrypt_key_bytes = b64decode(encrypt_key_raw)
        if len(encrypt_key_bytes) != 32:
            raise ValueError('If defined, ENCRYPT_KEY must be exactly 32 bytes wide, in base64')
    else:
        encrypt_key_bytes = get_random_bytes(32)

    # Construct an encryption object.
    def _new_crypter():
        return AES.new(encrypt_key_bytes, AES.MODE_SIV)

    def _obfuscate(plaintext: _Plaintext) -> _Obfuscated:
        encrypted, digest = _new_crypter().encrypt_and_digest(plaintext)
        return b''.join([encrypted, digest])

    def _deobfuscate(obfuscated: _Obfuscated) -> _Plaintext:
        plaintext, digest = obfuscated[:-16], obfuscated[-16:]
        return _new_crypter().decrypt_and_verify(plaintext, digest)

    return _obfuscate, _deobfuscate


# Key for general obfuscation
OBFUSCATE, DEOBFUSCATE = _derive_obfuscation_oracles()


def _derive_signature_scheme() -> (_SignatureOracle, _VerificationOracle):
    """Derive our signing and verification oracles.

    Note that though this actually runs legitimate cryptography, the key is optionally derived from
    an environment variable, which is not secure.

    As a design principle, this method attempts to propagate keys only via function closures, which
    make the key state actually inaccessible to code in other parts of our binary.

    :return: signature and verification oracles.
    """

    # Read the ephemeral signing key file and delete its corresponding file.
    signing_key_filename = from_env.get_str('SIGNING_PRIVATE_KEY_FILENAME', None)
    if signing_key_filename:
        del environ['SIGNING_PRIVATE_KEY_FILENAME']

    # Read the raw DER.
    if signing_key_filename:
        with open(signing_key_filename) as signing_key_file:
            signing_key = ECC.import_key(signing_key_file.read())
        remove(signing_key_filename)
        if signing_key.curve != 'P-256':
            raise ValueError(
                'If defined, SIGNING_PRIVATE_KEY_FILENAME must point to a valid P256 private key')
    else:
        signing_key = ECC.generate(curve='P-256')

    def _sign(msg: _Plaintext) -> _Signature:
        signer = DSS.new(signing_key, 'fips-186-3', encoding='binary')
        return signer.sign(SHA256.new(msg))

    public_key = signing_key.public_key()

    def _verify(msg: _Plaintext, sig: _Signature) -> _IsValid:
        verifier = DSS.new(public_key, 'fips-186-3', encoding='binary')
        return verifier.verify(SHA256.new(msg), sig)

    return _sign, _verify


SIGN_INSECURE, VERIFY_INSECURE = _derive_signature_scheme()
