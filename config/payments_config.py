from config.lib import from_env, throw

USE_FEATURE_COLLECTIVE_DUES: bool = from_env.get_bool(
    'ENABLE_FEATURE_COLLECTIVE_DUES', False
)
STRIPE_SECRET_KEY: str = from_env.get_str(
    'COLLECTIVE_DUES_STRIPE_SECRET_KEY',
    throw if USE_FEATURE_COLLECTIVE_DUES else 'sk_test_yourtestsecretkeygoeshere',
)
STRIPE_LOCAL_PRODUCT_ID: str = from_env.get_str(
    'COLLECTIVE_DUES_STRIPE_LOCAL_PRODUCT_ID',
    throw if USE_FEATURE_COLLECTIVE_DUES else 'prod_yourlocalproductidgoeshere',
)
STRIPE_NATIONAL_PRODUCT_ID: str = from_env.get_str(
    'COLLECTIVE_DUES_STRIPE_NATIONAL_PRODUCT_ID',
    throw if USE_FEATURE_COLLECTIVE_DUES else 'prod_yournatlproductidgoeshere',
)
