from typing import Dict, NamedTuple

from config.lib import from_env


# TODO: Migrate to Python 3.8 to use TypedDicts
class ChapterVars(NamedTuple):
    name: str
    short_name: str
    address: str
    banner_image_url: str


KNOWN_CHAPTERS: Dict[str, ChapterVars] = {
    'san-francisco': ChapterVars(
        'DSA San Francisco',
        'DSA SF',
        '350 Alabama St #9, San Francisco, CA 94122',
        (
            'https://dsasf.org/wp-content/themes/paperback/images/dsasf-logo-cleaned.svg'
        ),
    ),
    'silicon-valley': ChapterVars(
        'Silicon Valley DSA',
        'SV DSA',
        '1346 The Alameda Ste 7-84, San Jose, CA 95126-2699',
        (
            'https://siliconvalleydsa.org/wp-content/uploads/2020/03/sv_dsa_logo_text_red.png'
        ),
    ),
}

CHAPTER_ID: str = from_env.get_str('CHAPTER_ID', 'san-francisco')
CHAPTER_NAME: str = from_env.get_str('CHAPTER_NAME', KNOWN_CHAPTERS[CHAPTER_ID].name)
CHAPTER_SHORT_NAME: str = from_env.get_str(
    'CHAPTER_SHORT_NAME', KNOWN_CHAPTERS[CHAPTER_ID].short_name
)
CHAPTER_ADDRESS: str = from_env.get_str(
    'CHAPTER_ADDRESS', KNOWN_CHAPTERS[CHAPTER_ID].address
)
CHAPTER_BANNER_IMAGE_URL: str = from_env.get_str(
    'CHAPTER_BANNER_IMAGE_URL', KNOWN_CHAPTERS[CHAPTER_ID].banner_image_url
)
