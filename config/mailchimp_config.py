from config.lib import from_env, throw

MAILCHIMP_ENABLED = from_env.get_bool('MAILCHIMP_ENABLED', False)
MAILCHIMP_SECRET = from_env.get_str('MAILCHIMP_SECRET', throw if MAILCHIMP_ENABLED else None)

NEWSLETTER_LIST_ID = "a1d5d677c7"
ELIGIBILITY_LIST_ID = "423377e753"
