from membership.services.crm.crm_record import CrmRecord


class TestCrmRecord:

    def test_from_raw_record_should_extract_fields(self):
        record_id = 'ref1234'
        email = 'test@example.com'
        first_name = 'Test'
        last_name = 'User'
        phone = '(123)-456-7890'
        source = 'website'
        join_date = '01/01/2020'
        do_not_email = True
        do_not_call = True

        raw_record = {
            'id': record_id,
            'fields': {
                'Email': email,
                'First Name': first_name,
                'Last Name': last_name,
                'Phone': phone,
                'Onboarding Source': source,
                'Join Date': join_date,
                'Do Not Email': do_not_email,
                'Do Not Call': do_not_call
            }
        }

        crm_record = CrmRecord.from_raw_record(raw_record)

        assert crm_record.record_id == record_id
        assert crm_record.email == email
        assert crm_record.first_name == first_name
        assert crm_record.last_name == last_name
        assert crm_record.phone == phone
        assert crm_record.onboarding_source == source
        assert crm_record.join_date == join_date

    def test_to_json_should_contruct_airtable_schema(self):
        record_id = 'ref1234'
        email = 'test@example.com'
        first_name = 'Test'
        last_name = 'User'
        phone = '(123)-456-7890'
        source = 'website'
        join_date = '01/01/2020'
        do_not_email = True
        do_not_call = True

        raw_record = CrmRecord(
            record_id=record_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            onboarding_source=source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        ).to_json()

        assert raw_record['Email'] == email
        assert raw_record['First Name'] == first_name
        assert raw_record['Last Name'] == last_name
        assert raw_record['Phone'] == phone
        assert raw_record['Onboarding Source'] == source
        assert raw_record['Join Date'] == join_date
        assert raw_record['Do Not Email'] == do_not_email
        assert raw_record['Do Not Call'] == do_not_call

    def test_equality(self):
        left = TestCrmRecord.create_record()
        right = TestCrmRecord.create_record()

        assert left == right

        right.record_id = 'test'
        assert left != right

        right = TestCrmRecord.create_record()
        right.email = 'test'
        assert left != right

        right = TestCrmRecord.create_record()
        right.first_name = 'test'
        assert left != right

        right = TestCrmRecord.create_record()
        right.last_name = 'test'
        assert left != right

        right = TestCrmRecord.create_record()
        right.phone = 'test'
        assert left != right

        right = TestCrmRecord.create_record()
        right.onboarding_source = 'test'
        assert left != right

        right = TestCrmRecord.create_record()
        right.join_date = 'test'
        assert left != right

    @staticmethod
    def create_record() -> CrmRecord:
        record_id = 'ref1234'
        email = 'test@example.com'
        first_name = 'Test'
        last_name = 'User'
        phone = '(123)-456-7890'
        source = 'website'
        join_date = '01/01/2020'
        do_not_email = True
        do_not_call = True

        return CrmRecord(
            record_id=record_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            onboarding_source=source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )
