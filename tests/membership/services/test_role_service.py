from membership.database.base import engine, metadata, Session
from membership.database.models import Role, Member
from membership.services.role_service import RoleService


class TestRoleService:
    def setup(self):
        metadata.create_all(engine)

    def teardown(self):
        metadata.drop_all(engine)

    def test_add_committee_active_role_to_member(self):
        session = Session()
        member = Member(email_address="fake123@example.com")
        session.add(member)
        session.commit()
        role_service = RoleService()
        role1 = Role(member_id=member.id, role="member", committee_id=None)
        role2 = Role(member_id=member.id, role="member", committee_id=14)
        role3 = Role(member_id=member.id, role="active", committee_id=14)
        results = []
        for r in (role1, role2, role3):
            res = role_service.add(
                session,
                r.member_id,
                r.role,
                r.committee_id,
            )
            assert res
            results.append(res)
        added_roles = role_service.find_by_member_id(session, member.id)
        assert sorted([r.id for r in added_roles]) == results

    def test_duplicate_general_member_role_only_added_once(self):
        session = Session()
        member = Member(email_address="fake123@example.com")
        session.add(member)
        session.commit()
        role_service = RoleService()
        role = Role(member_id=member.id, role="member", committee_id=None)
        res1 = role_service.add(session, role.member_id, role.role, role.committee_id)
        assert res1
        res2 = role_service.add(session, role.member_id, role.role, role.committee_id)
        assert not res2
        res3 = role_service.find_by_member_id(session, role.member_id)
        assert [r.id for r in res3] == [res1]

    def test_remove(self):
        session = Session()
        member = Member(email_address="fake123@example.com")
        session.add(member)
        session.commit()
        role_service = RoleService()
        role = Role(member_id=member.id, role="member", committee_id=None)
        res1 = role_service.add(session, role.member_id, role.role, role.committee_id)
        assert res1
        res2 = role_service.remove(
            session,
            role.member_id,
            role.role,
            role.committee_id,
        )
        assert res2
