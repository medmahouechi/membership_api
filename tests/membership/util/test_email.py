from config import (
    EMAIL_CONNECTOR,
    SUPER_USER_EMAIL,
    SUPER_USER_FIRST_NAME,
    SUPER_USER_LAST_NAME,
)
from membership.database.models import Member
from membership.util.email import (
    get_email_connector,
    send_welcome_email,
    parse_combined_sender,
    MailjetEmailConnector
)

email_connector = get_email_connector(EMAIL_CONNECTOR)


class TestEmail:
    def test_send_welcome_email_no_error(self):
        member = Member(
            first_name=SUPER_USER_FIRST_NAME,
            last_name=SUPER_USER_LAST_NAME,
            email_address=SUPER_USER_EMAIL,
            do_not_email=False,
        )
        send_welcome_email(email_connector, member, 'https://dsasftest.org/')

    def test_parse_combined_sender(self):
        sender = "Jón Þór Jonsson <jon@jonsson.com>"
        sender_name, sender_email = parse_combined_sender(sender)

        assert sender_name == "Jón Þór Jonsson"
        assert sender_email == "jon@jonsson.com"

    def test_parse_invalid_sender(self):
        sender = "Something completely different"
        sender_name, sender_email = parse_combined_sender(sender)

        assert sender_name == None
        assert sender_email == None


def test_mailjet_parse_existing_templates():
    connector = MailjetEmailConnector()

    template = """<p>Hi %recipient.name%! Here's a test email!</p>"""
    converted = """<p>Hi [[var:name]]! Here's a test email!</p>"""
    assert connector._parse_existing_templates(template) == converted


def test_mailjet_parse_existing_templates_empty():
    connector = MailjetEmailConnector()

    template = """<p>Hi recipient! Here's a test email!</p>"""
    converted = """<p>Hi recipient! Here's a test email!</p>"""
    assert connector._parse_existing_templates(template) == converted
