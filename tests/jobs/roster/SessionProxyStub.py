from typing import Any

from jobs.roster.session_proxy import SessionProxy
from membership.database.models import NationalMembershipData, Member


class SessionProxyStub(SessionProxy):
    entities = []
    idx = 0

    def add(self, x: Any):
        self.entities.append(x)

    def flush(self):
        for ent in self.entities:
            if isinstance(ent, NationalMembershipData) or isinstance(ent, Member):
                ent.id = self.idx
                self.idx += 1

    def commit(self):
        pass

    def rollback(self):
        pass

    def delete(self, x: Any):
        self.entities.remove(x)

    def is_modified(self, x: Any) -> bool:
        return False
