from __future__ import annotations
from datetime import datetime
import re
from typing import Dict, List, Optional

import phonenumbers
from phonenumbers import NumberParseException, PhoneNumberFormat

from jobs.roster.import_results import ImportResults
from membership.database.models import (
    Identity,
    IdentityProviders,
    Member,
    NationalMembershipData,
    PhoneNumber,
    UnionMemberStatus,
)
from membership.schemas import JsonObj
from membership.util.time import CHAPTER_TIME_ZONE


class CSVKeys:
    AK_ID = "AK_ID"
    DSA_ID = "DSA_ID"
    EMAIL = "Email"
    ACTIVE = "Memb_status"
    JOIN_DATE = "Join_Date"
    EXP_DATE = "Xdate"
    FIRST_NAME = "first_name"
    MIDDLE_NAME = "middle_name"
    LAST_NAME = "last_name"
    ZIP = "Billing_Zip"
    ADDRESS_1 = "Billing_Address_Line_1"
    ADDRESS_2 = "Billing_Address_Line_2"
    CITY = "Billing_City"
    STATE = "Billing_State"
    COUNTRY = "Country"
    HOME_PHONE = "Home_Phone"
    MOBILE_PHONE = "Mobile_Phone"
    WORK_PHONE = "Work_Phone"
    DO_NOT_CALL = "Do_Not_Call"
    UNION_MEMBER = "union_member"
    UNION_NAME = "union_name"
    UNION_LOCAL = "union_local"


class CSVRecord:

    def __init__(
            self,
            ak_id: str,
            dsa_id: Optional[str],
            first_name: str,
            middle_name: Optional[str],
            last_name: str,
            email: str,
            active: bool,
            address_line_1: Optional[str],
            address_line_2: Optional[str],
            city: Optional[str],
            zipcode: Optional[str],
            country: Optional[str],
            do_not_call: bool,
            join_date: Optional[datetime],
            dues_paid_until: Optional[datetime],
            home_nums: List[str],
            cell_nums: List[str],
            work_nums: List[str],
            union_member: Optional[str],
            union_name: Optional[str],
            union_local: Optional[str],
    ):
        self.ak_id = ak_id
        self.dsa_id = dsa_id
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.email = email
        self.active = active
        self.address_line_1 = address_line_1
        self.address_line_2 = address_line_2
        self.city = city
        self.zipcode = zipcode
        self.country = country
        self.do_not_call = do_not_call
        self.join_date = join_date
        self.dues_paid_until = dues_paid_until
        self.home_nums = home_nums
        self.cell_nums = cell_nums
        self.work_nums = work_nums
        self.union_member = union_member
        self.union_name = union_name
        self.union_local = union_local

    @staticmethod
    def parse_csv_row(row: Dict[str, str]) -> 'CSVRecord':
        record = CSVRecord(
            ak_id=row[CSVKeys.AK_ID],  # required
            dsa_id=row.get(CSVKeys.DSA_ID, "").strip() or None,
            first_name=row[CSVKeys.FIRST_NAME],  # required,
            middle_name=row.get(CSVKeys.MIDDLE_NAME, "").strip() or None,
            last_name=row[CSVKeys.LAST_NAME],  # required
            email=row[CSVKeys.EMAIL],  # required
            active=row.get(CSVKeys.ACTIVE) == "M",
            address_line_1=row.get(CSVKeys.ADDRESS_1, "").strip() or None,
            address_line_2=row.get(CSVKeys.ADDRESS_2, "").strip() or None,
            city=row.get(CSVKeys.CITY, "").strip() or None,
            zipcode=CSVRecord.parse_zipcode(row.get(CSVKeys.ZIP, "").strip() or None),
            country=row.get(CSVKeys.COUNTRY, "").strip() or "United States",
            do_not_call=row.get(CSVKeys.DO_NOT_CALL) == "TRUE",
            join_date=CSVRecord.format_optional_date(row.get(CSVKeys.JOIN_DATE)),
            dues_paid_until=CSVRecord.format_optional_date(row.get(CSVKeys.EXP_DATE)),
            home_nums=row.get(CSVKeys.HOME_PHONE, "").split(","),
            cell_nums=row.get(CSVKeys.MOBILE_PHONE, "").split(","),
            work_nums=row.get(CSVKeys.WORK_PHONE, "").split(","),
            union_member=row.get(CSVKeys.UNION_MEMBER, "").strip() or None,
            union_name=row.get(CSVKeys.UNION_NAME, "").strip() or None,
            union_local=row.get(CSVKeys.UNION_LOCAL, "").strip() or None,
        )

        return record

    def to_json(self) -> JsonObj:
        return {
            "ak_id": self.ak_id,
            "dsa_id": self.dsa_id,
            "email": self.email,
            "first_name": self.first_name,
            "middle_name": self.middle_name,
            "last_name": self.last_name,
            "active": self.active,
            "address_line_1": self.address_line_1,
            "address_line_2": self.address_line_2,
            "city": self.city,
            "zipcode": self.zipcode,
            "country": self.country,
            "do_not_call": self.do_not_call,
            "join_date": self.join_date.strftime("%Y-%m-%d"),
            "dues_paid_until": self.dues_paid_until.strftime("%Y-%m-%d"),
            "union_member": self.union_member,
            "union_name": self.union_name,
            "union_local": self.union_local,
        }

    def to_national_record(self) -> NationalMembershipData:
        membership = NationalMembershipData(
            ak_id=self.ak_id,
            dsa_id=self.dsa_id,
            first_name=self.first_name,
            middle_name=self.middle_name,
            last_name=self.last_name,
            active=self.active,
            address_line_1=self.address_line_1,
            address_line_2=self.address_line_2,
            city=self.city,
            zipcode=self.zipcode,
            country=self.country,
            do_not_call=self.do_not_call,
            join_date=self.join_date,
            dues_paid_until=self.dues_paid_until,
            union_member=self.union_member_status,
            union_name=self.union_name,
            union_local=self.union_local,
        )

        return membership

    def to_chapter_record(self) -> Member:
        normalized_email = Member.normalize_email(self.email)

        return Member(
            first_name=self.first_name,
            last_name=self.last_name,
            email_address=self.email,
            normalized_email=normalized_email,
            do_not_call=self.do_not_call
        )

    @property
    def union_member_status(self) -> Optional[UnionMemberStatus]:
        status = self.union_member.strip() if isinstance(self.union_member, str) else ""

        if status == "Yes, current union member":
            return UnionMemberStatus.active
        elif status == "Yes, retired union member":
            return UnionMemberStatus.former
        elif status.startswith("No, not a union member"):
            return UnionMemberStatus.none
        else:
            return None

    def get_identities(self, member_id: int) -> List[Identity]:
        ids: List[Identity] = []

        identity = Identity(
            member_id=member_id,
            provider_name=IdentityProviders.AK,
            provider_id=self.ak_id)
        ids.append(identity)

        if self.dsa_id:
            identity = Identity(
                member_id=member_id,
                provider_name=IdentityProviders.DSA,
                provider_id=self.dsa_id)
            ids.append(identity)

        return ids

    def get_phone_numbers(self, member_id: int, results: 'ImportResults') -> List[PhoneNumber]:
        phone_numbers: List[PhoneNumber] = []

        for n in self.cell_nums:
            cell_num = CSVRecord.format_phone(n, results)
            if cell_num and not any(p.number == cell_num for p in phone_numbers):
                num = PhoneNumber(member_id=member_id, number=cell_num, name="Cell")
                phone_numbers.append(num)
        for n in self.home_nums:
            home_num = CSVRecord.format_phone(n, results)
            if home_num and not any(p.number == home_num for p in phone_numbers):
                num = PhoneNumber(member_id=member_id, number=home_num, name="Home")
                phone_numbers.append(num)
        for n in self.work_nums:
            work_num = CSVRecord.format_phone(n, results)
            if work_num and not any(p.number == work_num for p in phone_numbers):
                num = PhoneNumber(member_id=member_id, number=work_num, name="Work")
                phone_numbers.append(num)

        return phone_numbers

    def format_phone(num: str, results: 'ImportResults') -> Optional[str]:
        raw_num = re.sub("[(). +-]*", lambda m: "", num)
        if not raw_num:
            return None
        full_phone_number = (
            f"+1 {num}"
            if len(raw_num) == 10 or (len(raw_num) == 11 and raw_num[0] == "1")
            else raw_num
        )
        try:
            return phonenumbers.format_number(
                phonenumbers.parse(full_phone_number),
                PhoneNumberFormat.INTERNATIONAL,
            )
        except NumberParseException:
            results.phone_numbers_failed.add(num)
            return None

    def parse_zipcode(raw: str) -> str:
        if not raw:
            return raw

        raw_zip = raw.replace("-", "")
        raw_zip_len = len(raw_zip)
        if not raw_zip.isdigit() or raw_zip_len != 5 and raw_zip_len != 9:
            raise ValueError("Invalid zipcode format: '{}'".format(raw_zip))
        elif raw_zip_len == 9:
            return raw_zip[:5] + "-" + raw_zip[5:]
        else:
            return raw_zip

    def format_optional_date(dts: Optional[str]) -> Optional[datetime]:
        if not dts:
            return None
        try:
            # Date Format 2 (Feb 2020)
            dt = datetime.strptime(dts, "%Y-%m-%d")
        except ValueError:
            try:
                # Date Time Format 2 (Feb 2020)
                dt = datetime.strptime(dts, "%Y-%m-%d %H:%M")
            except ValueError:
                try:
                    # Date Format 1 (May 2018)
                    dt = datetime.strptime(dts, "%m/%d/%y %H:%M")
                except ValueError:
                    # Date Time Format 1 (March 2018)
                    dt = datetime.strptime(dts, "%m/%d/%Y")
        return dt.astimezone(CHAPTER_TIME_ZONE)
