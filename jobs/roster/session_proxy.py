from typing import Any

from sqlalchemy import orm


# An easy to mock proxy that forwards calls to a sql alchemy session object.
class SessionProxy:
    def __init__(self, session: orm.Session):
        self.session = session

    def add(self, x: Any):
        self.session.add(x)

    def flush(self):
        self.session.flush()

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()

    def delete(self, x: Any):
        self.session.delete(x)

    def refresh(self, x: Any):
        self.session.refresh(x)

    def is_modified(self, x: Any) -> bool:
        return self.session.is_modified(x)
