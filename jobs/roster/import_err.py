from jobs.roster import csv_record


class ImportErr:

    def __init__(self, error: Exception, csv_record: csv_record):
        self.error = error
        self.csv_record = csv_record

    def to_json(self):
        return {
            "error_message": str(self.error),
            "csv_record": self.csv_record.to_json()
        }
