"""Unique constraint for meeting name

Revision ID: 88e598dc35fa
Revises: 51cddc530569
Create Date: 2019-07-06 22:42:00.170995

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '88e598dc35fa'
down_revision = '51cddc530569'
branch_labels = None
depends_on = None


def upgrade():
    op.create_unique_constraint(None, 'meetings', ['name'])


def downgrade():
    op.drop_constraint(None, 'meetings', type_='unique')
