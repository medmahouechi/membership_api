"""Merge migration heads

Revision ID: 64a7384c1514
Revises: 4784d950b7f9, 1a0ae01dbf3d
Create Date: 2020-07-08 21:49:51.344628

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '64a7384c1514'
down_revision = ('4784d950b7f9', '1a0ae01dbf3d')
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
