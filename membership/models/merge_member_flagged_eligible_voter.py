class MergeMemberFlaggedEligibleVoter:
    def __init__(self,
                 election_id: int,
                 did_keep_id_vote: bool,
                 did_remove_id_vote: bool):
        self.election_id = election_id
        self.did_keep_id_vote = did_keep_id_vote
        self.did_remove_id_vote = did_remove_id_vote
