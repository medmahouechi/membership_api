from membership.util.delegator import SimpleDelegator


class MemberAsEligibleToVote(SimpleDelegator):

    def __init__(self, *args,
                 is_eligible: bool,
                 message: str,
                 dues_are_paid: bool,
                 meets_attendance_criteria: bool,
                 active_in_committee: bool):
        super().__init__(*args)
        self.is_eligible = is_eligible
        self.message = message
        self.dues_are_paid = dues_are_paid
        self.meets_attendance_criteria = meets_attendance_criteria
        self.active_in_committee = active_in_committee
