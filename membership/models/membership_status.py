from enum import Enum


class MembershipStatus(Enum):
    GOOD_STANDING = 0
    LEAVING_STANDING = 1
    OUT_OF_STANDING = 2
    UNKNOWN = 3
    NOT_A_MEMBER = 4
    SUSPENDED = 5
    LEFT_CHAPTER = 6
