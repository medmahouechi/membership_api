from membership.database.models import ProxyTokenState


class MergeMemberFlaggedProxyToken:

    def __init__(
            self,
            member_id: int,
            meeting_id: int,
            receiving_member_id: int,
            state: ProxyTokenState):
        self.member_id = member_id
        self.meeting_id = meeting_id
        self.receiving_member_id = receiving_member_id
        self.state = state
