from .eligible_member import MemberAsEligibleToVote


class AttendeeAsEligibleToVote(MemberAsEligibleToVote):

    def __init__(self,
                 attendee, message,
                 dues_are_paid,
                 meets_attendance_criteria,
                 active_in_committee):
        super().__init__(
            attendee.member,
            is_eligible=bool(attendee.eligible_to_vote),
            message=message,
            dues_are_paid=dues_are_paid,
            meets_attendance_criteria=meets_attendance_criteria,
            active_in_committee=active_in_committee
        )
        self.meeting = attendee.meeting
