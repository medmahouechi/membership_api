import csv
import logging
from io import StringIO
from typing import Optional, List  # NOQA: F401

from flask import Blueprint, jsonify, request
from sqlalchemy.exc import IntegrityError, SQLAlchemyError
from sqlalchemy.orm import joinedload
from sqlalchemy.sql.functions import now

import config  # NOQA
from jobs.auth0_invites import SendAuth0Invites
from jobs.import_membership import ImportNationalMembership
from jobs.roster.session_cache import RosterImportSessionCache
from membership.database.models import (  # NOQA: F401
    InterestTopic,
    Meeting,
    Member,
    Role,
    PhoneNumber,
    AdditionalEmailAddress,
    Tag)
from membership.database.base import Session
from membership.models import AuthContext, UnauthorizedException
from membership.repos import MeetingRepo
from membership.schemas.rest.members import (
    format_member_basics,
    format_member_full,
    format_member_query_result,
    format_eligible_member_list,
    format_merge_member_flags,
    format_phone_numbers_full,
    format_email_addresses,
    add_member_role_request_schema,
    update_interests_request_schema,
    update_notes_request_schema,
    update_member_details_request_schema,
    add_phone_number_request_schema,
    delete_phone_number_request_schema,
    add_email_address_request_schema,
    delete_email_address_request_schema, add_or_remove_tags)
from membership.services import (
    MeetingService,
    AttendeeService,
    MemberService,
    InterestTopicsService,
    TopicNotFoundError,
    PhoneNumberService)
from membership.services.crm.crm_service import CrmService
from membership.services.eligibility import SanFranciscoEligibilityService
from membership.services.errors import ValidationError
from membership.services.mailchimp.mailchimp_action_generator import MailchimpActionGenerator
from membership.services.mailchimp.mailchimp_service import MailchimpService
from membership.services.onboarding.onboarding_service import OnboardingService
from membership.services.onboarding.onboarding_status import OnboardingStatus
from membership.services.role_service import RoleService
from membership.services.slack.slack_service import SlackService
from membership.util.logging import LoggerIO
from membership.web.auth import requires_auth, CreateAuth0UserError
from membership.web.util import (
    BadRequest,
    NotFound,
    Conflict,
    ServerError,
    requires_json,
    validate_request,
    Unauthorized, Ok)

logger = logging.getLogger(__name__)
member_api = Blueprint("member_api", __name__)
meeting_service = MeetingService()
meeting_repository = MeetingRepo(Meeting)
attendee_service = AttendeeService()
eligibility_service = SanFranciscoEligibilityService(
    meeting_repository, attendee_service
)
role_service = RoleService()
interest_topics_service = InterestTopicsService
number_service = PhoneNumberService()
mailchimp_service = MailchimpService()
mailchimp_action_gen = MailchimpActionGenerator()
crm_service = CrmService(number_service)
slack_service = SlackService()
onboarding_service = OnboardingService(
    mailchimp_service, mailchimp_action_gen, crm_service, slack_service)
member_service = MemberService(eligibility_service, role_service, onboarding_service)


@member_api.route("/member/list", methods=["GET"])
@requires_auth("admin")
def get_members(ctx: AuthContext):
    if request.args.get("page_size", None):
        page_size = int(request.args.get("page_size", None))
        cursor = request.args.get("cursor", None)
        member_query_result = member_service.query(
            cursor=cursor, page_size=page_size, query_str=None, session=ctx.session
        )
        return jsonify(format_member_query_result(member_query_result, ctx.az))
    else:
        # TODO(ageiduschek): Kill this code path once we support
        # pagination results on the frontend
        return jsonify(get_members_list_deprecated(ctx))


@member_api.route("/member/search", methods=["GET"])
@requires_auth()
def search_members(ctx: AuthContext):
    if not any(role.role == "admin" for role in ctx.requester.roles):
        raise UnauthorizedException()

    page_size = int(request.args.get("page_size", 10))
    query = request.args.get("query", None)
    cursor = request.args.get("cursor", None)
    member_query_result = member_service.query(
        cursor=cursor, page_size=page_size, query_str=query, session=ctx.session
    )
    return jsonify(format_member_query_result(member_query_result, ctx.az))


@member_api.route("/member", methods=["GET"])
@requires_auth()
def get_member(ctx: AuthContext):
    member = format_member_basics(ctx.requester, ctx.az)
    return jsonify(member)


# TODO(ageiduschek): Kill this code path once we support
# pagination results on the frontend
def get_members_list_deprecated(ctx: AuthContext):
    members_result = member_service.all(ctx.session)
    eligible_members = eligibility_service.members_as_eligible_to_vote(
        ctx.session, members_result.members
    )
    return format_eligible_member_list(ctx.az)(eligible_members)


@member_api.route("/member/details", methods=["GET"])
@requires_auth()
def get_member_details(ctx: AuthContext):
    if ctx.az.member_id != ctx.requester.id:
        return Unauthorized("User cannot request other members' personal information.")

    member = format_member_full(ctx.session, ctx.requester, ctx.az)
    return jsonify(member)


@member_api.route("/admin/member/details", methods=["GET"])
@requires_auth("admin")
def get_member_info(ctx: AuthContext):
    other_member = ctx.session.query(Member).get(request.args["member_id"])
    return jsonify(format_member_full(ctx.session, other_member, ctx.az))


@member_api.route("/member", methods=["POST"])
@requires_auth("admin")
@requires_json
def add_member(ctx: AuthContext):
    email_address: Optional[str] = request.json["email_address"].strip() or None
    first_name: Optional[str] = request.json["first_name"].strip() or None
    last_name: Optional[str] = request.json["last_name"].strip() or None
    make_member = request.json.get("give_chapter_member_role", False)
    if not email_address and not (first_name and last_name):
        return BadRequest(
            "You must supply either an email or a full name to add a member"
        )

    upgraded = False
    try:
        member = member_service.create_member(
            ctx.session,
            email_address=email_address,
            first_name=first_name,
            last_name=last_name,
            transaction_mode=False
        )

        if make_member:
            upgraded = member_service.upgrade_to_national_member(member, True, ctx.session)

        results = onboarding_service.onboard_member(member, "Admin", make_member)
        logger.info(results.to_json())
    except IntegrityError:
        return Conflict(f"Member with email already exists: {email_address}")
    except CreateAuth0UserError as e:
        logger.error(e.message)
        return ServerError("Error creating user in Auth0")

    return jsonify(
        {
            "status": "success",
            "data": {
                "member": format_member_basics(member, ctx.az),
                "account_created": results.auth0_result == OnboardingStatus.SUCCESS,
                "email_sent": results.portal_email_result == OnboardingStatus.SUCCESS,
                "role_created": upgraded
            },
        }
    )


@member_api.route("/member", methods=["PATCH", "PUT"])
@requires_auth()
@requires_json
def update_member(ctx: AuthContext):
    member = ctx.requester

    if request.json.get("do_not_call", None) is not None:
        member.do_not_call = request.json["do_not_call"]
    if request.json.get("do_not_email", None) is not None:
        member.do_not_email = request.json["do_not_email"]

    ctx.session.add(member)
    ctx.session.commit()

    return jsonify(
        {
            "status": "success",
            "data": format_member_full(ctx.session, ctx.requester, ctx.az),
        }
    )


@member_api.route("/member/upgrade/<int:member_id>", methods=["PUT"])
@requires_auth("admin")
def upgrade_to_member(ctx: AuthContext, member_id: int):
    found_member = member_service.find_by_id(ctx.session, member_id)
    if not found_member:
        return NotFound()

    upgraded = member_service.upgrade_to_national_member(found_member, True, ctx.session)
    results = onboarding_service.onboard_member(found_member, "Upgrade Existing", True)

    return jsonify(
        {
            "status": "success",
            "data": {
                "auth0_account_created": results.auth0_result.status == OnboardingStatus.SUCCESS,
                "email_sent": results.portal_email_result == OnboardingStatus.SUCCESS,
                "role_created": upgraded,
            },
        }
    )


@member_api.route("/member/downgrade/<int:member_id>", methods=["POST"])
@requires_auth("admin")
def cancel_dues_override_member(ctx: AuthContext, member_id: int):
    member = member_service.find_by_id(ctx.session, member_id)
    if not member:
        return NotFound()

    member.dues_paid_overridden_on = None
    ctx.session.commit()

    return jsonify(
        {
            "status": "success"
        }
    )


@member_api.route("/member/suspend/<int:member_id>", methods=["POST"])
@requires_auth("admin")
def suspend_member(ctx: AuthContext, member_id: int):
    member = member_service.find_by_id(ctx.session, member_id)
    if not member:
        return NotFound()

    member.suspended_on = now()
    ctx.session.commit()

    return jsonify(
        {
            "status": "success"
        }
    )


@member_api.route("/member/exit/<int:member_id>", methods=["POST"])
@requires_auth("admin")
def exit_member(ctx: AuthContext, member_id: int):
    member = member_service.find_by_id(ctx.session, member_id)
    if not member:
        return NotFound()

    member.left_chapter_on = now()
    member.do_not_email = True
    member.do_not_call = True
    ctx.session.commit()

    return jsonify(
        {
            "status": "success"
        }
    )


@member_api.route("/admin/member", methods=["PATCH"])
@requires_auth("admin")
@requires_json
@validate_request(update_member_details_request_schema)
def update_member_details(ctx: AuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])
    json_body = request.get_json(silent=True, force=True)

    if json_body.get("first_name") is not None:
        member.first_name = json_body.get("first_name")
    if json_body.get("last_name") is not None:
        member.last_name = json_body.get("last_name")
    if json_body.get("pronouns") is not None:
        member.pronouns = json_body.get("pronouns")
    if json_body.get("biography") is not None:
        member.biography = json_body.get("biography")
    if json_body.get("do_not_call") is not None:
        member.do_not_call = json_body.get("do_not_call")
    if json_body.get("do_not_email") is not None:
        member.do_not_email = json_body.get("do_not_email")

    ctx.session.add(member)
    ctx.session.commit()

    return jsonify(
        {
            "status": "success",
            "data": format_member_full(ctx.session, ctx.requester, ctx.az),
        }
    )


@member_api.route("/import", methods=["PUT"])
@requires_auth("admin")
def import_members(ctx: AuthContext):
    import_file = request.files.get("file").read().decode("utf-8")
    rows = csv.DictReader(StringIO(import_file))
    job = ImportNationalMembership()
    output = LoggerIO(
        logging.INFO, logger=logging.getLogger("membership.web.import_members")
    )

    csv_records = job.parse_records(iter(rows), output)

    cache = RosterImportSessionCache(Session)

    results = job.import_all(
        csv_records,
        cache,
        SendAuth0Invites(),
        out=output)

    cache = cache.refresh_session()
    member_cache = {m.id: m for m in cache.chapter_records}
    return jsonify({"status": "success", "data": results.to_json(member_cache)})


@member_api.route("/admin", methods=["POST"])
@requires_auth("admin")
@requires_json
def make_admin(ctx: AuthContext):
    member = (
        ctx.session.query(Member)
        .filter_by(email_address=request.json["email_address"])
        .one()
    )
    committee_id = request.json["committee"] if request.json["committee"] > 0 else None
    role = Role(member_id=member.id, role="admin", committee_id=committee_id)
    ctx.session.add(role)
    ctx.session.commit()
    return jsonify({"status": "success"})


@member_api.route("/member/role", methods=["POST"])
@requires_auth()
@requires_json
@validate_request(add_member_role_request_schema)
def add_role(ctx: AuthContext):
    member_id = request.json.get("member_id", ctx.requester.id)
    cid = request.json["committee_id"]
    role_name = request.json["role"]
    committee_id = cid if cid is not None and cid > 0 else None
    ctx.az.verify_admin(committee_id)

    if not role_service.add(
        ctx.session,
        member_id=member_id,
        role=role_name,
        committee_id=committee_id,
        commit=True,
    ):
        return Conflict("Member already has this role")
    else:
        return jsonify({"status": "success"})


@member_api.route("/member/role", methods=["DELETE"])
@requires_auth()
@requires_json
def remove_role(ctx: AuthContext):
    member_id = request.json.get("member_id", ctx.requester.id)
    role = request.json["role"]
    cid = request.json["committee_id"]
    committee_id = cid if cid is not None and cid > 0 else None
    ctx.az.verify_admin(committee_id)

    if not role_service.remove(
        ctx.session, member_id=member_id, role=role, committee_id=committee_id,
    ):
        return NotFound("Member does not have this role")
    else:
        return jsonify({"status": "success"})


@member_api.route("/member/attendee", methods=["POST"])
@requires_auth()
@requires_json
def add_attendee(ctx: AuthContext):
    meeting_id = request.json["meeting_id"]
    member_id = request.json.get("member_id", ctx.requester.id)
    meeting = ctx.session.query(Meeting).filter_by(id=meeting_id).one()
    ctx.az.verify_admin(meeting.committee_id)

    attendee_service.attend_meeting(
        member_id, meeting, ctx.session, verified=True, return_none_on_error=True
    )

    if meeting.is_voting_meeting:
        # Attendance may be recorded retroactively; although the method
        # which sets eligibility during attandance will raise an error if
        # the meeting has already ended, we will quietly ignore it and
        # leave the eligibility status untouched.
        try:
            eligibility_service.update_eligibility_to_vote_at_attendance(
                ctx.session, member=member_id, meeting=meeting
            )
        except ValidationError as e:
            if e.key != SanFranciscoEligibilityService.ErrorCodes.MEETING_ENDED:
                return BadRequest(e.message)

    return jsonify({"status": "success"})


@member_api.route("/member/<int:member_to_keep_id>/merge", methods=["POST"])
@requires_auth("admin")
def merge_member(member_to_keep_id: int, ctx: AuthContext):
    """
    Performs a merge of two members.

    This requires modifying or deleting a number of records across many tables. Any record belonging
    to the member_to_remove that also "duplicates" a record belonging to the member_to_keep will be
    removed. If said record is not a "duplicate" it will be kept and member_id will be updated to
    the member_to_keep_id. Each table has a unique definition for when a record "duplicates"
    another, see comments for method "perform_merge_for_all_tables" for further details.

    The merge will only be executed if 'force' is true or if there are no sensitive deletes that
    would be committed. Currently, the only sensitive deletes are:
        - There is a duplicate EligibleVoter record, as removing this record would alter vote
          counts.
        - There is a duplicate ProxyToken record, as removing this record would alter vote counts.

    If 'force' is false and a delete is flagged, the merge will not happen and the flagged items
    will be returned. Otherwise, method will return None.

    Parameters:
    member_id_to_keep (int): The member_id being merged into. These records will remain after the
                             merge.
    member_to_remove_id (int): The member_id of the user that will be merged (i.e. will no longer
                               exist after merge operation is complete)
    force (bool): If false, will not perform merge if any deteles are flagged. If true, will perform
                  merge regardless of any flagged deletes.
    """
    member_to_remove_arg = request.args.get("member_to_remove_id")
    force_arg = request.args.get("force")

    if member_to_remove_arg is None:
        return BadRequest(
            "Caller must supply a member_to_remove_id as a query parameter."
        )
    if force_arg is None:
        force_arg = "False"

    try:
        member_to_remove_id = int(member_to_remove_arg)
    except ValueError:
        return BadRequest(
            "Caller must supply an integer id for member_to_remove_id parameter."
        )
    force = force_arg.lower() == "true"

    try:
        data = member_service.merge_member_fetch_data(
            member_to_keep_id, member_to_remove_id, ctx.session
        )
        if data.member_to_keep is None:
            return NotFound(
                "The provided member to keep does not exist. Merge could not be performed."
            )
        if data.member_to_remove is None:
            return NotFound(
                "The provided member to remove does not exist. Merge could not be performed."
            )

        if not force:
            flags = member_service.check_for_merge_member_flags(
                member_to_keep_id, member_to_remove_id, data
            )
            if flags is not None:
                return jsonify(format_merge_member_flags(flags))

        res = member_service.perform_merge_for_all_tables(
            member_to_keep_id, member_to_remove_id, data, ctx.session
        )
        return jsonify({"number_of_updates": res[0], "number_of_deletions": res[1]})
    except Exception as e:
        return ServerError(str(e))


@member_api.route("/admin/member/notes", methods=["PUT"])
@requires_auth("admin")
@requires_json
@validate_request(update_notes_request_schema)
def update_notes(ctx: AuthContext):
    notes: str = request.json["notes"]
    member = ctx.session.query(Member).get(request.args["member_id"])
    try:
        member.notes = notes.strip()
        ctx.session.add(member)
        ctx.session.commit()
        return jsonify({"notes": member.notes})
    except SQLAlchemyError:
        ctx.session.rollback()
        return ServerError("Error when updating notes for member")


@member_api.route("/admin/member/phone_numbers", methods=["POST"])
@requires_auth("admin")
@requires_json
@validate_request(add_phone_number_request_schema)
def add_phone_number(ctx: AuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])
    json_response = request.get_json()

    if member is None:
        return NotFound("Member does not exist")

    try:
        phone_number = PhoneNumber(
            member_id=member.id,
            number=json_response.get("phone_number"),
            name=json_response.get("name"),
        )
        ctx.session.add(phone_number)
        ctx.session.commit()
        return jsonify(format_phone_numbers_full(member.phone_numbers))
    except IntegrityError:
        ctx.session.rollback()
        return Conflict(
            f"Member with phone number already exists: {request.json['phone_number']}"
        )


@member_api.route("/admin/member/phone_numbers", methods=["DELETE"])
@requires_auth("admin")
@requires_json
@validate_request(delete_phone_number_request_schema)
def delete_phone_number(ctx: AuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])

    if member is None:
        return NotFound("Member does not exist")

    record = (
        ctx.session.query(PhoneNumber)
        .filter_by(member_id=member.id, number=request.json["phone_number"])
        .one_or_none()
    )

    if record is None:
        return NotFound(
            f"Member does not have this phone number: {request.json['phone_number']}"
        )

    ctx.session.delete(record)
    ctx.session.commit()
    return jsonify(format_phone_numbers_full(member.phone_numbers))


@member_api.route("/admin/member/email_addresses", methods=["POST"])
@requires_auth("admin")
@requires_json
@validate_request(add_email_address_request_schema)
def add_email_address(ctx: AuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])
    json_response = request.get_json()

    if member is None:
        return NotFound("Member does not exist")

    try:
        email_address = AdditionalEmailAddress(
            member_id=member.id,
            email_address=json_response.get("email_address"),
            name=json_response.get("name"),
            preferred=json_response.get("preferred") or False,
        )
        ctx.session.add(email_address)
        ctx.session.commit()
        return jsonify(format_email_addresses(member.additional_email_addresses))
    except IntegrityError:
        ctx.session.rollback()
        return Conflict(
            f"Member with email_address already exists: {request.json['email_address']}"
        )


@member_api.route("/admin/member/email_addresses", methods=["DELETE"])
@requires_auth("admin")
@requires_json
@validate_request(delete_email_address_request_schema)
def delete_email_address(ctx: AuthContext):
    member = ctx.session.query(Member).get(request.args["member_id"])

    if member is None:
        return NotFound("Member does not exist")

    record = (
        ctx.session.query(AdditionalEmailAddress)
        .filter_by(member_id=member.id, email_address=request.json["email_address"])
        .one_or_none()
    )

    if record is None:
        return NotFound(
            f"Member does not have this email address: {request.json['email_address']}"
        )

    ctx.session.delete(record)
    ctx.session.commit()
    return jsonify(format_email_addresses(member.additional_email_addresses))


@member_api.route("/member/interests", methods=["GET"])
@requires_auth("admin")
def get_interests(ctx: AuthContext):
    member = ctx.requester
    topics: List[InterestTopic] = interest_topics_service.list_member_interest_topics(
        ctx, member
    )
    return jsonify({"topics": [topic.name for topic in topics]})


@member_api.route("/member/interests", methods=["PUT"])
@requires_auth("admin")
@requires_json
@validate_request(update_interests_request_schema)
def update_interests(ctx: AuthContext):
    topics = set(request.json["topics"])

    logger.info(f"Saving member interests: {topics}")
    member = ctx.requester

    try:
        interests = interest_topics_service.create_interests(
            ctx, member.id, topics, create_if_missing=False,
        )
        ctx.session.commit()
        return jsonify({"topics": [i.topic.name for i in interests]})
    except TopicNotFoundError as e:
        return BadRequest(
            f"`topics` array contained one or more unknown topics: {e.topics}"
        )


@member_api.route("/member/<int:member_id>/tags", methods=["PUT"])
@requires_auth("admin")
@requires_json
@validate_request(add_or_remove_tags)
def add_tags(member_id: int, ctx: AuthContext):
    member = ctx.session.query(Member) \
        .options(joinedload(Member.tags)) \
        .get(member_id)

    if not member:
        return NotFound("Member does not exist")

    existing = {tag for tag in member.tags}

    try:
        tags_to_add = [tag for tag in request.json["tags"] if tag not in existing]
        for tag in tags_to_add:
            db_tag = ctx.session.query(Tag).filter(Tag.name == tag).first()

            if not db_tag:
                new_tag = Tag(name=tag)
                ctx.session.add(new_tag)
                member.tags.append(new_tag)

        ctx.session.commit()

        return Ok()
    except SQLAlchemyError:
        ctx.session.rollback()
        return ServerError("Error when updating notes for member")


@member_api.route("/member/<int:member_id>/tags", methods=["DELETE"])
@requires_auth("admin")
@requires_json
@validate_request(add_or_remove_tags)
def remove_tags(member_id: int, ctx: AuthContext):
    member = ctx.session.query(Member) \
        .options(joinedload(Member.tags)) \
        .get(member_id)

    if not member:
        return NotFound("Member does not exist")

    tags_to_remove = request.json["tags"]

    try:
        for tag in tags_to_remove:
            tag_rec = next(filter(lambda t: t.name == tag, member.tags), None)
            if tag_rec:
                member.tags.remove(tag_rec)

        ctx.session.commit()

        return Ok()
    except SQLAlchemyError:
        ctx.session.rollback()
        return ServerError("Error when updating notes for member")
