import json
import logging
from datetime import date, datetime
from decimal import Decimal
from typing import Optional, Dict, Callable
from functools import wraps

from flask import Response, request
from flask.json import JSONEncoder
from jsonschema import validate, ValidationError

from membership.schemas import Json

logger = logging.getLogger(__name__)


class Ok(Response):
    def __init__(self, data: Optional[Json] = None) -> None:
        body = None if data is None else json.dumps(data)
        mimetype = None if body is None else 'application/json'
        super(Ok, self).__init__(
            body, status=200, mimetype=mimetype)


class Created(Response):
    def __init__(self, data: Optional[Json] = None) -> None:
        body = None if data is None else json.dumps(data)
        mimetype = None if body is None else 'application/json'
        super(Created, self).__init__(
            body, status=201, mimetype=mimetype)


class Accepted(Response):
    def __init__(self, data: Optional[Json] = None) -> None:
        body = None if data is None else json.dumps(data)
        mimetype = None if body is None else 'application/json'
        super(Accepted, self).__init__(
            body, status=202, mimetype=mimetype)


class BadRequest(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(BadRequest, self).__init__(
            json.dumps(payload), status=400, mimetype='application/json')


class Unauthorized(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(Unauthorized, self).__init__(
            json.dumps(payload), status=401, mimetype='application/json')


class PaymentRequired(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(PaymentRequired, self).__init__(
            json.dumps(payload), status=402, mimetype='application/json')


class NotFound(Response):
    def __init__(self, err: str = 'Not Found') -> None:
        payload = {'status': 'failed', 'err': err}
        super(NotFound, self).__init__(
            json.dumps(payload), status=404, mimetype='application/json')


class Conflict(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(Conflict, self).__init__(
            json.dumps(payload), status=409, mimetype='application/json')


class UnprocessableEntity(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(UnprocessableEntity, self).__init__(
            json.dumps(payload), status=422, mimetype='application/json')


class RateLimitExceeded(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(RateLimitExceeded, self).__init__(
            json.dumps(payload), status=429, mimetype='application/json')


class ServerError(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(ServerError, self).__init__(
            json.dumps(payload), status=500, mimetype='application/json')


class ServiceUnavailable(Response):
    def __init__(self, err: str) -> None:
        payload = {'status': 'failed', 'err': err}
        super(ServiceUnavailable, self).__init__(
            json.dumps(payload), status=503, mimetype='application/json')


def requires_json(f: Callable):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not request.is_json:
            return BadRequest("Expected 'Content-Type:application/json' header")
        else:
            return f(*args, **kwargs)
    # Blueprint methods have to have unique names
    decorated.__name__ = f.__name__
    return decorated


def validate_request(schema: Dict):
    def decorator(f: Callable):
        @wraps(f)
        def decorated(*args, **kwargs):
            try:
                validate(request.json, schema)
                return f(*args, **kwargs)
            except ValidationError as ve:
                return BadRequest(f"Request did not match expected schema: {ve}")
        return decorated
    return decorator


def validate_response(schema: Dict):
    def decorator(f: Callable):
        @wraps(f)
        def decorated(*args, **kwargs):
            try:
                response = f(*args, **kwargs)
                validate(response.json, schema)
                return response
            except ValidationError as ve:
                return ServerError(f"Response did not match expected schema: {ve}")
        return decorated
    return decorator


# TODO: We should always convert datetime to a number of millis from epoch
class CustomEncoder(JSONEncoder):
    """ Custom encoder class converts Decimals to strings and datetime objects into ISO
    formatted strings. """

    def default(self, obj):
        if isinstance(obj, Decimal):
            return str(obj)
        if isinstance(obj, datetime):
            return obj.isoformat()
        if isinstance(obj, date):
            return obj.isoformat()
        return JSONEncoder.default(self, obj)
