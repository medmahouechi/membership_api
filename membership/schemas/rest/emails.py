from membership.database.models import Email
from membership.schemas import JsonObj


def format_email(email: Email) -> JsonObj:
    return {
        'id': email.id,
        'email_address': email.email_address,
        'forwarding_addresses': [f.forward_to for f in email.forwarding_addresses],
    }
