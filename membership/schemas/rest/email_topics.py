from overrides import overrides

from membership.database.models import InterestTopic
from membership.schemas import JsonObj
from membership.schemas.rest.model import RestModel


class EmailTopicsRest(RestModel[InterestTopic]):
    model = InterestTopic
    schema = {
        'type': 'object',
        'properties': {
            'id': {
                'type': ['integer', 'string'],
            },
            'name': {
                'type': 'string',
            },
        },
        'required': ['name']
    }

    @classmethod
    @overrides
    def format(cls, topic: InterestTopic) -> JsonObj:
        return {
            'id': topic.id,
            'name': topic.name,
        }
