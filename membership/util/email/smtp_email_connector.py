import smtplib
import ssl

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from overrides import overrides
from typing import Any, Dict, List

from config import (
    SMTP_HOST,
    SMTP_PORT,
    SMTP_USERNAME,
    SMTP_PASSWORD,
    USE_EMAIL,
)
from membership.database.models import Member
from .email_connector import EmailConnector


class SmtpEmailConnector(EmailConnector):
    @overrides
    def send_member_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[Member, Dict[str, str]],
        tag: str = '',
        add_unsubscribe_link: bool = True,
    ):
        """
        Create an email campaign from the sender using the given MailGun email template formatted
        string and substitute the recipient variables for every email key in the dict.

        NOTE: If `recipient_variables` is empty, then the email is sent to nobody, otherwise the
        email is sent to every key of the dictionary given.

        :param sender: the from field of the email to be received
        :param subject: the subject line of the email
        :param email_template: the full template with all variables / recipient variables declared
        :param recipient_variables: all the recipients of this email by email address with
                                    all variables associated with this address in the values
        """
        filtered_recipient_variables = {
            member.email_address: obj
            for member, obj in recipient_variables.items()
            if not member.do_not_email
        }
        if add_unsubscribe_link:
            email_template = (
                f"{email_template}\n\n\n"
                "to unsubscribe click here: %tag_unsubscribe_url%"
            )
        self.send_emails(
            sender_name=sender_name,
            sender_email=sender_email,
            subject=subject,
            email_template=email_template,
            recipient_variables=filtered_recipient_variables,
            to_emails=list(filtered_recipient_variables.keys()),
        )

    @overrides
    def send_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[str, Dict[Any, str]],
        to_emails: List[str],
        tag: str = '',
    ):
        if USE_EMAIL:
            try:
                server = smtplib.SMTP(SMTP_HOST, SMTP_PORT)
                if SMTP_PORT == 587:
                    context = ssl.create_default_context()
                    server.starttls(context=context)
                if SMTP_USERNAME is not None and SMTP_PASSWORD is not None:
                    server.login(SMTP_USERNAME, SMTP_PASSWORD)
                for email in to_emails:
                    message = MIMEMultipart("alternative")
                    message["From"] = f"{sender_name} <{sender_email}>"
                    message["To"] = email
                    # perform variable expansion/substitution
                    variables = recipient_variables.get(email, {})
                    subj = subject
                    html = email_template
                    for key, value in variables.items():
                        subj = subj.replace(f"%recipient.{key}%", value)
                        html = html.replace(f"%recipient.{key}%", value)
                    message["Subject"] = subj
                    message.attach(MIMEText(html, "html"))
                    server.sendmail(message["From"], message["To"], message.as_string())
            finally:
                server.quit()
