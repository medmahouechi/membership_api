from datetime import datetime
from typing import List, Optional, Type

from membership.database.models import Meeting
from membership.util.time import get_current_time
from sqlalchemy import and_, orm


class MeetingRepo:

    def __init__(self, meeting_table: Type[Meeting]) -> None:
        self.meeting_table = meeting_table

    def most_recent(
        self,
        session: orm.Session,
        limit: int = 3,
        since: Optional[datetime] = None
    ) -> List[Meeting]:
        if since is None:
            since = get_current_time()

        query = session \
            .query(self.meeting_table) \
            .filter(and_(
                self.meeting_table.is_voting_meeting,
                self.meeting_table.start_time != None,
                self.meeting_table.end_time < since,
            )) \
            .order_by(
                self.meeting_table.start_time.asc(),
            )[-limit:]

        return list(query)
