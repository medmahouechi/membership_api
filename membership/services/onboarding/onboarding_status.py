from enum import Enum


class OnboardingStatus(Enum):
    SUCCESS = 0
    OPTED_OUT = 1
    ALREADY_ADDED = 2
    ERROR = 3
    NOT_IMPLEMENTED = 4
    SKIPPED = 5
