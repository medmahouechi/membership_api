import re
from typing import List, Optional

import phonenumbers
from membership.database.models import PhoneNumber
from membership.database.statements import insert_or_ignore
from phonenumbers import PhoneNumberFormat
from sqlalchemy import orm


class PhoneNumberService:
    def create_number(self, number: str, name: str = "Cell") -> Optional[PhoneNumber]:
        """Creates a phone number with a normalized format."""
        raw_number = re.sub("[(). +-]*", lambda m: "", number)
        if not raw_number:
            return None
        full_phone_number = f"+1 {number}" if len(raw_number) == 10 else number
        formatted_num = phonenumbers.format_number(
            phonenumbers.parse(full_phone_number), PhoneNumberFormat.INTERNATIONAL
        )
        return PhoneNumber(number=formatted_num, name=name)

    def upsert(self, session: orm.Session, member_id: int, number: PhoneNumber) -> None:
        number.member_id = member_id
        self.add_all(session, [number])

    def add_all(self, session: orm.Session, numbers: List[PhoneNumber]) -> None:
        statement = insert_or_ignore(session, PhoneNumber)
        session.execute(
            statement,
            [
                {"member_id": pn.member_id, "number": pn.number, "name": pn.name}
                for pn in numbers
            ],
        )
        session.commit()

    def best_phone_for_calling(self, numbers: List[PhoneNumber]) -> PhoneNumber:
        cell = next(iter([pn for pn in numbers if pn.name == "Cell"]), None)
        if cell:
            return cell

        kiosk = next(
            iter([pn for pn in numbers if pn.name == "Kiosk" or pn.name == "WebForm"]),
            None,
        )
        if kiosk:
            return kiosk

        home = next(iter([pn for pn in numbers if pn.name == "Home"]), None)
        if home:
            return home

        return next(iter(numbers), None)
