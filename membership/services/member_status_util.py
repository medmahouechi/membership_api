from datetime import datetime
from typing import Optional

import pytz
from dateutil.relativedelta import relativedelta

from membership.database.models import Member, NationalMembershipData
from membership.models.membership_status import MembershipStatus


class MemberStatusUtil:

    @staticmethod
    def get_member_status(member: Member,
                          membership: Optional[NationalMembershipData]) -> MembershipStatus:
        # Certain statuses take priority regardless of whether the individual's dues payments.
        if member.suspended_on:
            return MembershipStatus.SUSPENDED
        elif member.left_chapter_on:
            return MembershipStatus.LEFT_CHAPTER

        # In some cases, a member won't have a national record. This includes:
        #   - the member lives outside of SF zip codes (national won't send us their record)
        #   - the member just signed up (there is a lag between sign up and us getting the record)
        # In these cases an admin will manually set a dues_paid_overridden_on flag which signifies
        # the members dues have been confirmed. If this isn't set, we can safely assume they aren't
        # member.
        elif not membership and member.onboarded_on:
            return MembershipStatus.UNKNOWN
        elif not membership:
            return MembershipStatus.NOT_A_MEMBER

        # If they have a national record on file, we use the dues_paid_until to check if they
        # are in standing. We give people a (roughly) one month grace period between when their
        # dues lapse and when we mark them out of standing (i.e. 'LEAVING_STANDING').

        dues_cutoff = MemberStatusUtil.calc_dues_cutoff()

        if membership.dues_paid_until < dues_cutoff and not member.dues_paid_overridden_on:
            return MembershipStatus.OUT_OF_STANDING
        elif dues_cutoff <= membership.dues_paid_until < datetime.now(pytz.utc) and \
                not member.dues_paid_overridden_on:
            return MembershipStatus.LEAVING_STANDING
        else:
            return MembershipStatus.GOOD_STANDING

        return MembershipStatus.NOT_A_MEMBER

    @staticmethod
    def calc_dues_cutoff() -> datetime:
        start_of_month = datetime.now(tz=pytz.utc).replace(day=1)
        return start_of_month - relativedelta(months=+1)
