from abc import ABC
from datetime import datetime
from typing import List, Optional, Union

from membership.database.models import Attendee, Meeting, Member
from membership.models import MemberAsEligibleToVote
from sqlalchemy import orm


# This class holds an abstract interface that encapsulates eligibility details.
# Eligibility requirements, semantics, and usage should be encoded within
# subclasses (e.g.; `SanFranciscoEligibilityService`), allowing for flexibility
# with integration in other parts of the application.
class EligibilityService(ABC):
    # Given a `Member`, returns if they are eligible to vote
    def is_member_eligible(
        self,
        session: orm.Session,
        member: Member,
        since: Optional[datetime] = None
    ) -> MemberAsEligibleToVote:
        requested_member = self.members_as_eligible_to_vote(session, [member], since)[0]
        return requested_member

    # Given a list of `Member`s, return a list of delgators that append
    # attributes for eligibility details.
    def members_as_eligible_to_vote(
        self,
        session: orm.Session,
        members: List[Member],
        since: Optional[datetime] = None
    ) -> List[MemberAsEligibleToVote]:
        raise NotImplementedError

    # Given a `Member` and `Meeting` (or an `Attendee`), find the relevant
    # `Attendee` and update any pertinent fields.
    def update_eligibility_to_vote_at_attendance(
        self,
        session: orm.Session,
        member: Optional[Union[Member, int, str]] = None,
        meeting: Optional[Union[Meeting, int, str]] = None,
        attendee: Optional[Union[Attendee, int, str]] = None
    ) -> None:
        raise NotImplementedError
