from typing import List

from membership.database.models import Identity
from sqlalchemy import insert, orm


class IdentityService:
    def add_all(self, session: orm.Session, identities: List[Identity],) -> None:
        for identity in identities:
            insert(Identity).prefix_with("IGNORE").values(
                member_id=identity.member_id,
                provider_name=identity.provider_name,
                provider_id=identity.provider_id,
            )
        session.commit()
