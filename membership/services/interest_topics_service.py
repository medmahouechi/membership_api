from typing import List, Optional, Dict, Set

from membership.database.models import Interest, InterestTopic, Member
from membership.models.authz import AuthContext


class TopicNotFoundError(Exception):
    def __init__(self, message, missing_topics):
        super().__init__(message)
        self.topics = missing_topics


class InterestTopicsService:
    @classmethod
    def create_interests(
        cls,
        ctx: AuthContext,
        member_id: int,
        topic_names: Set[str],
        create_if_missing: bool = True,
    ) -> List[Interest]:
        topics: List[InterestTopic] = []
        unknown_topic_names: List[str] = []
        for topic_name in topic_names:
            topic: Optional[InterestTopic] = ctx.session.query(InterestTopic).filter(
                InterestTopic.name == topic_name
            ).one_or_none()
            if topic is None:
                if create_if_missing:
                    topic = InterestTopic(name=topic_name)
                    ctx.session.add(topic)
                else:
                    unknown_topic_names.append(topic_name)

            topics.append(topic)

        # Raise exception if we find unknown topics or None topic entries in the DB
        if (
            len(unknown_topic_names) > 0
            or len([topic for topic in topics if topic is None]) > 0
        ):
            raise TopicNotFoundError("Unknown topics", unknown_topic_names)

        interests: List[Interest] = []
        member_interest_query = ctx.session.query(Interest).filter(
            Interest.member_id == member_id
        )
        for topic in topics:
            existing_interest: Optional[Interest] = member_interest_query.filter(
                Interest.topic == topic
            ).one_or_none()
            if existing_interest is None:
                new_interest: Interest = Interest(
                    member_id=member_id, topic_id=topic.id
                )
                ctx.session.add(new_interest)
                interests.append(new_interest)
            else:
                interests.append(existing_interest)

        ctx.session.commit()
        return interests

    @classmethod
    def create_interest_topics(
        cls, ctx: AuthContext, topic_names: List[str]
    ) -> List[InterestTopic]:
        topics: List[InterestTopic] = []
        for topic_name in topic_names:
            topic: Optional[InterestTopic] = ctx.session.query(InterestTopic).filter(
                InterestTopic.name == topic_name
            ).one_or_none()
            if topic is None:
                topic = InterestTopic(name=topic_name)
                ctx.session.add(topic)
            topics.append(topic)
        ctx.session.commit()
        return topics

    @classmethod
    def delete_interest_topics(
        cls, ctx: AuthContext, topic_names: List[str]
    ) -> Dict[str, bool]:
        topics: Dict[str, bool] = {}
        for topic_name in topic_names:
            topic: Optional[InterestTopic] = ctx.session.query(InterestTopic).filter(
                InterestTopic.name == topic_name
            ).one_or_none()
            if topic is None:
                topics[topic_name] = False
            else:
                ctx.session.delete(topic)
                topics[topic_name] = True
        ctx.session.commit()
        return topics

    @classmethod
    def list_member_interest_topics(
        cls, ctx: AuthContext, member: Member
    ) -> List[InterestTopic]:
        interests: List[Interest] = ctx.session.query(Interest).filter(
            Interest.member == member
        ).all()
        return [i.topic for i in interests]

    @classmethod
    def find_interest_topic(
        cls, ctx: AuthContext, topic_id: int
    ) -> Optional[InterestTopic]:
        return ctx.session.query(InterestTopic).get(topic_id)

    @classmethod
    def find_interest_topic_by_name(
        cls, ctx: AuthContext, topic_name: str
    ) -> Optional[InterestTopic]:
        return (
            ctx.session.query(InterestTopic)
            .filter(InterestTopic.name == topic_name)
            .one_or_none()
        )

    @classmethod
    def list_interest_topic_members(
        cls, ctx: AuthContext, topic: InterestTopic
    ) -> List[Member]:
        interests: List[Interest] = ctx.session.query(Interest).filter(
            Interest.topic == topic
        ).all()

        return [i.member for i in interests]

    @classmethod
    def list_all_interest_topics(cls, ctx: AuthContext) -> List[InterestTopic]:
        return ctx.session.query(InterestTopic).all()
