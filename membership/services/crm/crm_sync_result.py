from enum import Enum

from membership.schemas import JsonObj


class CrmSyncStatus(Enum):
    SUCCESS = 0
    ERROR = 1


class CrmSyncResult:

    def __init__(self, email: str, status: str, insert_count: int, update_count: int, message: str):
        self.email = email
        self.status = status
        self.insert_count = insert_count
        self.update_count = update_count
        self.message = message

    def is_success(self) -> bool:
        return self.status == CrmSyncStatus.SUCCESS

    def is_error(self) -> bool:
        return self.status == CrmSyncStatus.ERROR

    def to_json(self) -> JsonObj:
        return {
            'email': self.email,
            'status': self.status,
            'insert_count': self.insert_count,
            'update_count': self.update_count,
            'message': self.message
        }

    def __eq__(self, other):
        if not isinstance(other, CrmSyncResult):
            return False

        return (
            self.email == other.email
            and self.status == other.status
            and self.insert_count == other.insert_count
            and self.update_count == other.update_count
            and self.message == other.message
        )
