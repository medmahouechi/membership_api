from typing import Optional

from membership.schemas import JsonObj


class CrmRecord:

    def __init__(self,
                 record_id: Optional[str],
                 email: Optional[str],
                 first_name: Optional[str],
                 last_name: Optional[str],
                 phone: Optional[str],
                 onboarding_source: Optional[str],
                 join_date: Optional[str],
                 do_not_email: bool,
                 do_not_call: bool):
        self.record_id = record_id
        self.email = email.lower()
        self.first_name = first_name
        self.last_name = last_name
        self.phone = phone
        self.onboarding_source = onboarding_source
        self.join_date = join_date
        self.do_not_email = do_not_email
        self.do_not_call = do_not_call

    def to_json(self) -> JsonObj:
        return {
            'First Name': self.first_name,
            'Last Name': self.last_name,
            'Email': self.email,
            'Phone': self.phone,
            'Onboarding Source': self.onboarding_source,
            'Join Date': self.join_date,
            'Do Not Email': bool(self.do_not_email),
            'Do Not Call': bool(self.do_not_call)
        }

    def __eq__(self, other):
        if not isinstance(other, CrmRecord):
            return False

        return (
            self.record_id == other.record_id
            and self.email == other.email
            and self.first_name == other.first_name
            and self.last_name == other.last_name
            and self.phone == other.phone
            and self.onboarding_source == other.onboarding_source
            and self.join_date == other.join_date
            and self.do_not_email == other.do_not_email
            and self.do_not_call == other.do_not_call
        )

    def __ne__(self, other):
        return not self == other

    @staticmethod
    def from_raw_record(raw_record: JsonObj) -> 'CrmRecord':
        fields = raw_record['fields']

        record_id = raw_record['id']
        email = fields.get('Email', None)
        first_name = fields.get('First Name', None)
        last_name = fields.get('Last Name', None)
        phone = fields.get('Phone', None)
        onboarding_source = fields.get('Onboarding Source', None)
        join_date = fields.get('Join Date', None)
        do_not_email = fields.get('Do Not Email', None)
        do_not_call = fields.get('Do Not Call', None)

        return CrmRecord(
            record_id=record_id,
            email=email.lower() if email else None,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            onboarding_source=onboarding_source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )
