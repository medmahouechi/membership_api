import hashlib
from math import ceil
from typing import Dict, List

import requests

from config.mailchimp_config import MAILCHIMP_SECRET
from membership.services.mailchimp.mailchimp_action import UpdateTagsAction, MailchimpAction, \
    AddOrUpdateRecordAction, ActionResult
from membership.services.mailchimp.mailchimp_models import MailchimpMember


class MailchimpService:

    def get_list_members(self, list_id) -> Dict[str, MailchimpMember]:
        url = 'https://us16.api.mailchimp.com/3.0/lists/{0}/members?count={1}&offset={2}'

        member_lookup = {}
        page_size = 1000
        offset = 0
        count = 0  # hard cutoff to prevent any possible infinite loops

        while count < 20:
            paged_url = url.format(list_id, page_size, offset)

            r = requests.get(paged_url, auth=('_', MAILCHIMP_SECRET))

            if r.status_code != 200:
                raise Exception(
                    "call failed on %s with status %s: %s" % (paged_url, r.status_code, r.text))

            members = {
                rec.email_address: rec
                for rec in [MailchimpMember.from_json(m) for m in r.json()['members']]
            }
            member_lookup.update(members)

            total_records = r.json()['total_items']
            offset += len(members)
            count += 1

            if total_records and offset >= total_records:
                break

        return member_lookup

    """
    Executes a list of mailchimp actions and returns an accumulated list of api responses. If
    any individual action fails, it will keep processing the next actions. However, if there are
    multiple actions for the same email and one fails, no more actions for that email will be
    executed. Note, actions are processed in list order.
    """
    def apply_mailchimp_actions(self,
                                list_id: str,
                                actions: List[MailchimpAction]) -> List[ActionResult]:
        results = []
        email_failures = set()

        i = 0
        bucket = 0
        ten_percent = ceil(len(actions) / 10)
        for action in actions:
            print(action.email)

            if 1 < ten_percent < i:
                i = 0
                bucket += 1
                print("Processed %s%% of the records..." % (bucket * 10))

            email = action.email
            if email in email_failures:
                continue

            try:
                if isinstance(action, AddOrUpdateRecordAction):
                    results.append(self.update_member(list_id, action))

                if isinstance(action, UpdateTagsAction):
                    results.append(self.update_member_tags(list_id, action))
            except Exception as e:
                email_failures.add(email)
                results.append(ActionResult(action, e))

        return results

    def update_member(self, list_id: str, action: AddOrUpdateRecordAction) -> ActionResult:
        email = action.email
        payload = action.payload
        sub_hash = hashlib.md5(email.lower().encode('utf-8')).hexdigest()  # nosec

        response = requests.put(
            'https://us16.api.mailchimp.com/3.0/lists/{0}/members/{1}'.format(list_id, sub_hash),
            auth=('_', MAILCHIMP_SECRET),
            json=payload)

        return ActionResult(action, response)

    def update_member_tags(self, list_id: str, action: UpdateTagsAction) -> ActionResult:
        email = action.email
        json = action.payload
        sub_hash = hashlib.md5(email.lower().encode('utf-8')).hexdigest()  # nosec

        response = requests.post(
            'https://us16.api.mailchimp.com/3.0/lists/{0}/members/{1}/tags'.format(
                list_id, sub_hash),
            auth=('_', MAILCHIMP_SECRET),
            json=json)

        return ActionResult(action, response)
