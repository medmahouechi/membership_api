if __name__ == '__main__':
    import logging

    # Set the log level while booting the app in case anything goes wrong
    logging.getLogger().setLevel(logging.WARNING)

    from config import LOGGING_ROOT_LEVEL
    from membership.web.base_app import app

    # set the root logging level
    logging.getLogger().setLevel(LOGGING_ROOT_LEVEL)
    # silence noisy modules
    logging.getLogger('watchdog.observers').setLevel(logging.WARNING)

    app.run(host='0.0.0.0', threaded=True, port=8080)
